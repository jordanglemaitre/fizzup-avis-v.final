# Fizzup Avis

Voici les étapes à suivre pour récupérer mon projet

## Installation

Pour commencer nous allons ouvrir une console dans notre dossier de destination pour y effectué un

```bash
git clone https://gitlab.com/jordanglemaitre/fizzup-avis-v.final.git
```

Ensuite nous allons rentrer dans le fichier de notre projet pour installer les composants de ce dernier

```bash
cd fizzup-avis-v.final
```

```bash
composer install
```

Ainsi nous pouvons importer la base de donnée 

```bash
php bin/console doctrine:database:create
```

```bash
php bin/console make:migration
```

```bash
php bin/console doctrine:migrations:migrate
```

Nous allons aussi installer les composants pour les ASSETS

```bash
php bin/console ckeditor:install
```

```bash
php bin/console assets:install public
```

Finalement nous allons pouvoir lancer le serveur en executant la dernière commande

```bash
symfony server:start
```

