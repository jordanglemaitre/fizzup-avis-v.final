<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use App\Entity\Review;
use App\Entity\ReviewSearch;
use App\Form\ReviewSearchType;
use App\Form\ReviewType;
use App\Repository\ReviewRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilderInterface;

class HomeController extends AbstractController
{
    private $review;

    public function __construct(ReviewRepository $reviewRepository, ObjectManager $em)
    {
        $this->reviewRepository = $reviewRepository;
        $this->em = $em;
    }

    public function index(Request $request): Response
    {
        // Crée un avis
        $form = $this->createForm(ReviewType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $review = $form->getData();
            $this->em->persist($review);
            $this->em->flush();
            dump($review);
            return $this->redirectToRoute('home');
        }

        //Commentaire


        // Search
        $search = new ReviewSearch();
        $formSearch = $this->createForm(ReviewSearchType::class, $search);
        $formSearch->handleRequest($request);

        // Afficher les avis
        $avis = $this->reviewRepository->findAllReview($search);
        
        $urldate = $this->generateUrl('home', array('date' => 'asc'));
        

        return $this->render('home.html.twig', [
            'reviews' => $avis,
            'form' => $form->createView(),
            'urldate' => $urldate,
            'formSearch' => $formSearch->createView(),
            "GET" => $_GET
        ]);
    }
}