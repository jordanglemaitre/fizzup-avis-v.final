<?php
namespace App\Entity;

use DateTime;

class ReviewSearch {

    /**
     * @var boolean
     */
    private $reviewMax;

    /**
     * @var int|null
     */
    private $reviewSort;

    /**
     * @var DateTime
     */
    private $date;



    public function getReviewMax(): ?bool {
		return $this->reviewMax;
	}

	public function setReviewMax(bool $reviewMax) {
		$this->reviewMax = $reviewMax;
	}

    public function getReviewSort(): ?int {
        return $this->reviewSort;
    }

    public function setReviewSort(int $reviewSort) {
        $this->reviewSort = $reviewSort;
    }
	public function getDate(): ?DateTime {
		return $this->date;
	}

	public function setDate(DateTime $date) {
        $this->date = $date;
        
	}

    
}