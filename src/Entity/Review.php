<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReviewRepository")
 * @UniqueEntity("email")
 * @Vich\Uploadable()
 */
class Review
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */

    private $filename;

     /**

     * @var File|null
     * @Vich\UploadableField(mapping="review_image", fileNameProperty="filename")
     */
   private $imageFile;


    /**
     * @Assert\Regex ("/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/")
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min=1, max=5)
     */
    private $note;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    // public function getFilename() {
	// 	return $this->filename;
	// }

	// public function setFilename(?string $filename) {
    //     $this->$filename = $filename;
    //     return $this;
	// }

    // public function getImageFile() {
	// 	return $this->imageFile;
	// }

	// public function setImageFile(?File $imageFile) {
    //     $this->imageFile = $imageFile;
    //     if ($this->imageFile instanceof UploadedFile) {
    //         $this->created_at = new \DateTime('now');
    //     }
    //     return $this;
    // }
    
    /**
    * Get the value of imageFile
    *
    * @return  File
    */ 
   public function getImageFile()
   {
      return $this->imageFile;
   }

   /**
    * Set the value of imageFile
    *
    * @param  File  $imageFile
    *
    * @return  self
    */ 
   public function setImageFile($imageFile)
   {
      $this->imageFile = $imageFile;
      if ($this->imageFile instanceof UploadedFile) {
        $this->updated_At = new \DateTime('now');
    }
      return $this;
   }

    /**
     * Get the value of filename
     */ 
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set the value of filename
     *
     * @return  self
     */ 
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
