<?php

namespace App\Repository;

use App\Entity\Review;
use App\Entity\ReviewSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    /** 
     *    @return Review Par note
     */
    
    public function findByLevel($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.date = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAllReview(ReviewSearch $search) : Array
    {
        $query = $this->findReviewQuery();
        
        if ($search->getReviewMax() == "0") {
            $query = $query
                ->orderBy('r.note', 'ASC')
            ;
        }

        if ($search->getReviewMax() == "1") {
            $query = $query
                ->orderBy('r.note', 'DESC')
            ;
        }

        if ($search->getReviewSort() && $search->getReviewSort() != 0 ) {
            $query = $query
                ->andWhere('r.note = :reviewsort')
                ->setParameter('reviewsort', $search->getReviewSort())
            ;
        }

        if ($search->getDate()) {
            $date = new \DateTime(date_format($search->getDate(), 'Y-m-d'));
            $date = $date->format('Y-m-d');
            $query = $query
                ->andWhere('r.created_at LIKE :date')
                ->setParameter('date', '%'.$date.'%')
            ;
        }
        dump($query->getQuery());
        return $query->getQuery()->getResult();
    }

    public function findReviewQuery() : QueryBuilder
    {
        return $this->createQueryBuilder('r')
                    ->setMaxResults(10)
        ;
    }
    

    // /**
    //  * @return Review[] Returns an array of Review objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Review
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
