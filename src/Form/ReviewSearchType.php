<?php

namespace App\Form;

use App\Entity\ReviewSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reviewMax', ChoiceType::class, [
                'choices' => array(
                'Notes' => [
                    'Croissant' => "0",
                    'Décroissant' => "1"
                ]
             ),
                'required' => false,
                'label' => "Note: Croissant/Décroissant",
            ])
            ->add('reviewSort', ChoiceType::class, [
                'choices' => array(
                'Notes' => [
                    'Non' => "0",
                    '1 étoiles' => "1",
                    '2 étoiles' => "2",
                    '3 étoiles' => "3",
                    '4 étoiles' => "4",
                    '5 étoiles' => "5",
                ]
             ),
                'required' => false,
                'label' => "Par note",
                'attr' => [
                    'placeholder' => 'Avis par étoile'
                ]
            ])
            ->add('date', DateType::class, [
                'widget' => 'choice',
                'label' => "Par date",
                'data' => new \DateTime("now")
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReviewSearch::class,
            'method' => 'get',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix(){
        return '';
    }
}
