<?php

namespace App\Form;

use App\Entity\Review;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, [
                'label' => 'Email'
            ])
            ->add('pseudo', null, [
                'label' => 'Pseudo'
            ])
            ->add('note', null, [
                'label' => 'Note'
            ])
            ->add('commentaire', CKEditorType::class, [
                'label' => 'Commentaire'
            ])
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label' => 'Image de profil'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Review::class,
        ]);
    }
}
